from django.urls import path
from .views import ProductView

urlpatterns = [
    path('product/', ProductView.as_view({'get': 'list', 'post' : 'create' })),
    path('product/<int:pk>', ProductView.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}))
]