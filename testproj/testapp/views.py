# from django.shortcuts import render
from rest_framework import status
from rest_framework.response import Response
from .models import Product, Stock
from .serializer import ProductSerializer
from rest_framework.viewsets import ModelViewSet






class ProductView(ModelViewSet):
    model = Product
    serializer_class  = ProductSerializer
    queryset = Product.objects.all()
    Lookup_field = 'pk'

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        stock = Stock.objects.first()
        stock.products_count -= 1
        stock.save()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()

    
