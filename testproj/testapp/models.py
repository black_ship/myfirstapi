from django.db import models


class Product(models.Model):
    phone_number = models.CharField(verbose_name='номер телефона', blank=True, null=True, max_length=9, unique=True)
    name =  models.CharField(verbose_name='фио', max_length=512)
    total_bonus = models.IntegerField(verbose_name='общий бонус',blank=True, null=True, default=0)
    card_number = models.CharField(verbose_name='номер карты', blank=True, null=True, max_length=16)


class Stock(models.Model):
    products_count = models.PositiveIntegerField('количество продуктов', default=0)
    