from rest_framework import serializers
from .models import Product, Stock


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ('phone_number', 'name', 'total_bonus', 'card_number', 'id')
    
    def create(self, validated_data):
        instance = Product.objects.create(**validated_data)
        stock = Stock.objects.first()
        stock.products_count +=1
        stock.save()
        return instance
